using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnNavesVerdesController : MonoBehaviour {

	public 			GameObject 	Inimigo1;
	public 			GameObject 	Inimigo2;
	public	static	bool	 	PlayButton;
	public	static	bool		SpawnInimigosAtivo;

	// Cena1
	private			float 		navesRate1;
	private			float 		nextNave1;
	// Cena2
	private			float 		navesRate2;
	private			float 		nextNave2;
	// Cena3
	private			float 		navesRate3;
	private			float 		nextNave3;
	// Cena4
	private			float 		navesRate4;
	private			float 		nextNave4;


	void Awake () {
		SpawnInimigosAtivo = true;
	}

	void Start () {	
		
		// Velocidade do surgimento das naves inimigas:
		//Cena1	
		navesRate1 = Random.Range (1f,1.5f); 
		nextNave1 = 0;
		//Cena2	
		navesRate2 = Random.Range (0.7f,1.1f); 
		nextNave2 = 0;
		//Cena3	
		navesRate3 = Random.Range (0.5f,1f); 
		nextNave3 = 0;
		//Cena4	
		navesRate4 = Random.Range (0.1f,5f);
		nextNave4 = 0;
	}

	void Update () {
		if (PauseMenu.gamePausado == false) {
			Scene cenaAtual = SceneManager.GetActiveScene ();
	
			if (SpawnInimigosAtivo == true) {
				switch (cenaAtual.name) {
				case "cena1":
					if (Time.time > nextNave1) {
						nextNave1 = Time.time + navesRate1;
						SpawnInimigo1 ();
						SpawnInimigo2 ();
					}
					break;
				case "cena2":
					if (Time.time > nextNave2) {
						nextNave2 = Time.time + navesRate2;
						SpawnInimigo1 ();
						SpawnInimigo2 ();
					}
					break;
				case "cena3":
					if (Time.time > nextNave3) {
						nextNave3 = Time.time + navesRate3;	
						SpawnInimigo1 ();
						SpawnInimigo2 ();
					}
					break;
				case "cena4":
					if (Time.time > nextNave4) {
						nextNave4 = Time.time + navesRate4;
						SpawnInimigo1 ();
						SpawnInimigo2 ();	
					}
					break;
				default:
					break;
				}
			}

			if (CameraController.posicaoCamera >= 150) { // Checar If no player referente a um freeze sobre ele a partir desta posição
				SpawnInimigosAtivo = false;
			}

		}// If GamePausado

	}//Fecha Void Update

	void SpawnInimigo1 () {
		Vector3 posicaoRandom1 = new Vector3 (Random.Range (-8.12f, 8.53f), transform.position.y, 10);
		Instantiate (Inimigo1, posicaoRandom1, transform.rotation); // Quaternion.identity é para o objeto ficar perfeitamente alinhado com o mundo (sem rotação).
	}
	void SpawnInimigo2 () {
		Vector3 posicaoRandom2 = new Vector3 (Random.Range (-8.12f, 8.53f), transform.position.y, 10);
		Instantiate (Inimigo2, posicaoRandom2, transform.rotation); // Quaternion.identity é para o objeto ficar perfeitamente alinhado com o mundo (sem rotação).
	}
} //Fecha classe
