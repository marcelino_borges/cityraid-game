﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaFillColor : MonoBehaviour {

	public	Slider		BarraDeVida;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (BarraDeVida.value < 50) {
			this.gameObject.GetComponent <Image> ().color = Color.red;
		} else {
			this.gameObject.GetComponent <Image> ().color = Color.white;
		}
	}
}
