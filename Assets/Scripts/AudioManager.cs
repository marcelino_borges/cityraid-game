﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Policy;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

	public 			AudioSource 	efxSource; // Efeitos Sonoros
	public 			AudioSource 	musicSource; // Música de fundo
	public static 	AudioManager 	instance = null;
	public			Text 			audioText;
	public 			float 			lowPitchRange = -3f; // Pitch mais baixo do som 0.95
	public 			float 			highPitchRange = 3f; // Pitch mais alto do som 1.05
	//public			AnimationCurve	lowPassFilter; 

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject); 
		}

		DontDestroyOnLoad (gameObject);
	} // void Awake

	public void PlaySingle(AudioClip clip, float vol) {		
		efxSource.clip = clip;
		efxSource.volume = vol;
		efxSource.Play ();
	}

	public void RandomizeSfx (AudioClip clip, float vol) { // mudei de "(params AudioClip[] clips)
		float randomPitch = Random.Range (lowPitchRange, highPitchRange);
		efxSource.clip = clip;
		efxSource.volume = vol;
		efxSource.pitch = randomPitch;
		efxSource.Play ();
	}

	public void MutarSons() {
		if (efxSource.enabled == true) {
			efxSource.enabled = false;
			if (musicSource.enabled == true) {
				musicSource.volume = 0f;
				audioText.text = "AUDIO\nOFF";
			}
		} else if (efxSource.enabled == false) {
			efxSource.enabled = true;
			if (musicSource.enabled == true) {
				musicSource.volume = 0.1f;
				musicSource.Pause ();
			}
			efxSource.Pause ();
			audioText.text = "AUDIO\nON";
		}
	}

}// public class AudioManager

