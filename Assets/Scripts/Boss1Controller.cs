using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using JetBrains.Annotations;

public class Boss1Controller : MonoBehaviour {
	
	public			Rigidbody2D		rb;
	public 			Transform 		escape;
	public	 		GameObject 		balaBoss;
	public			Animator		Animador;
	public			Transform		BossPos1;
	public			Transform		BossPos2;
	public			Transform		BossPos3;
	public			Transform		BossPos4;
	public			AudioClip		AudioDanoInimigo;
	private			float 			timer;
	public			float 			fireRate;
	private			float 			nextFire;
	public			float 			minhaPosicaoX;
	public 			float 			BossPos1X;
	public 			float 			BossPos2X;
	public 			float 			BossPos3X;
	public 			float 			BossPos4X;
	public	static	bool			Boss1Ativo;
	public			bool 			mostraVidaBoss1;
	private			bool 			bossPodeAtirar;
	public	static	int				VidaBoss1;
	private			int 			DanoColPlayer;
	private			int 			DanoBala2;
	private			int 			DanoBala3;
	private			int 			DanoBala4;
	private			int				qtdTiros;


	void Start () {		

		Boss1Ativo = true;

		timer = 0;

		fireRate = 0.5f; 
		nextFire = 0;

		VidaBoss1 = 10;

		minhaPosicaoX = transform.position.x;
		BossPos1X = BossPos1.position.x;
		BossPos2X = BossPos2.position.x;
		BossPos3X = BossPos3.position.x;
		BossPos4X = BossPos4.position.x;

		DanoBala2 = 5;
		DanoBala3 = 10;
		DanoBala4 = 15;
		DanoColPlayer = 15;

		bossPodeAtirar = false;



	}

	void FixedUpdate () {
		timer += Time.time;

		//rb.AddForce (new Vector2 (1, 0));

		if (PauseMenu.gamePausado == false) {			
			if (CameraController.posicaoCamera >= CameraController.posicaoFimDeMapa) {
				
				IrPos2 ();

				minhaPosicaoX = transform.position.x;

				bossPodeAtirar = true;

				Atirar ();

				Invoke ("IrPos3", 5f); 

				minhaPosicaoX = transform.position.x;





				Invoke ("IrPos4", 5f); 

				minhaPosicaoX = transform.position.x;





			


			}

			if (VidaBoss1 <= 0) {
				Animador.SetTrigger ("morto");
				//StartCoroutine (MorrerEmudarCena());
				Invoke ("Morrer", 30f*Time.fixedDeltaTime); // Chama a função morrer após esperar 3 segundos!
			} 

			//rb.velocity = -transform.right * veloBoss1;
			//rb.velocity = -transform.right * veloInimigo;
			//		if (Input.GetButton ("Jump") && Time.time > nextFire) {
			//	nextFire = Time.time + fireRate;
			//	Instantiate (bala2, escape2.position, escape2.rotation);
			//if (Time.time > nextFire) {
			//	nextFire = Time.time + breakNextFire;
			//	Instantiate (bala2, escape2.position, escape2.rotation);
			//}
			//}
		} // If GamePausado
	} // Update

	void OnTriggerEnter2D (Collider2D colls) {
		if (CameraController.posicaoCamera >= 129.13) {
			if (CameraController.posicaoCamera >= 129.13) {
				switch (colls.gameObject.tag) {
				case "Player":
					VidaBoss1 -= DanoColPlayer;
					Animador.SetBool ("levadano", true);
					break;
				case "Bala2":
					VidaBoss1 -= DanoBala2; // Tira dano da vida do inimigo
					Animador.SetBool ("levadano", true);
					break;
				case "Bala3":
					VidaBoss1 -= DanoBala3; // Tira dano da vida do inimigo
					Animador.SetBool ("levadano", true);
					break;
				case "Bala4":
					VidaBoss1 -= DanoBala4; // Tira dano da vida do inimigo
					Animador.SetBool ("levadano", true);
					break;
				} //Switch			
			} //Fecha Void OnTriggerEnter2D
		} // If posicaoCamera
	} //Fecha Void OnCollision


	private void Atirar() {
		qtdTiros = 4;
		if (minhaPosicaoX == BossPos2X) {
			do {
				Debug.Log ("Entrei no loop " + qtdTiros + " vezes.");
				if (Time.time > nextFire) {
					nextFire = Time.time + fireRate;	
					Instantiate (balaBoss, escape.position, escape.rotation);
				
				}
				qtdTiros--;
			} while (qtdTiros > 0);

		}

	}

	void Morrer() {		
		Destroy (gameObject); // Destrói a nave após a colisão com o player ou com a bala após 15s
	}

	void IrPos2 () {
		if (minhaPosicaoX < BossPos2X) {
			transform.position = Vector3.MoveTowards (transform.position, BossPos2.position, 8f * Time.fixedDeltaTime);
			//rb.AddForce (new Vector2 (1, 0) * 200f); // Funcionando - Verificar Linear Drag no Inspector
		} 
	}

	void IrPos3 () {
		if (minhaPosicaoX < BossPos3X) {
			transform.position = Vector3.MoveTowards (transform.position, BossPos3.position, 8f * Time.fixedDeltaTime);
			//rb.AddForce (new Vector2 (1, 0) * 200f); // Funcionando - Verificar Linear Drag no Inspector
		}
	}

	void IrPos4 () {
		if (minhaPosicaoX < BossPos4X) {
			transform.position = Vector3.MoveTowards (transform.position, BossPos4.position, 8f * Time.fixedDeltaTime);
			//rb.AddForce (new Vector2 (1, 0) * 200f); // Funcionando - Verificar Linear Drag no Inspector
		}	
	}
	 
	void LevaDanoFalse() {
		Animador.SetBool ("levadano", false);
	}
}
