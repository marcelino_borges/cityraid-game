﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class WinStage : MonoBehaviour {
	public			Animator	AnimadorWinStage;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Boss1Controller.VidaBoss1 <= 0) {
			AnimadorWinStage.SetTrigger ("stage1win");
			PlayerController.PlayerAtivo = false;

		} 
		
	}

	public void BtNextParaCena2() {
		
		SceneManager.LoadScene ("cena2");
	}
	public void BtNextParaCena3() {

		SceneManager.LoadScene ("cena3");
	}
	public void BtNextParaCena4() {

		SceneManager.LoadScene ("cena4");
	}
	public void BtNextParaCena5() {

		SceneManager.LoadScene ("cena5");
	}
	public void BtNextParaCena6() {

		SceneManager.LoadScene ("cena6");
	}
}
