﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnVidaColetavel : MonoBehaviour {

	public 			GameObject 	coracaoColetavel;
	// Cena1
	private			float 		coracaoRate1;
	private			float 		nextCoracao1;
	// Cena2
	private			float 		coracaoRate2;
	private			float 		nextCoracao2;
	// Cena3
	private			float 		coracaoRate3;
	private			float 		nextCoracao3;
	// Cena4
	private			float 		coracaoRate4;
	private			float 		nextCoracao4;


	void Start () {	
		

		//Cena1	
		coracaoRate1 = Random.Range (13f,15f); // Velocidade do surgimento das naves inimigas
		nextCoracao1 = 0;
		//Cena2	
		coracaoRate2 = Random.Range (0.7f,1.1f); // Velocidade do surgimento das naves inimigas
		nextCoracao2 = 0;
		//Cena3	
		coracaoRate3 = Random.Range (0.5f,1f); // Velocidade do surgimento das naves inimigas
		nextCoracao3 = 0;
		//Cena4	
		coracaoRate4 = Random.Range (0.1f,5f); // Velocidade do surgimento das naves inimigas
		nextCoracao4 = 0;
	}

	void Update () {
		Scene cenaAtual = SceneManager.GetActiveScene ();

		if (SpawnNavesVerdesController.SpawnInimigosAtivo == true) {
			switch (cenaAtual.name) {
			case "cena1":
				if (Time.time > nextCoracao1) {
					nextCoracao1 = Time.time + coracaoRate1;
					SpawnCoracaoColetavel ();

				}
				break;
			case "cena2":
				if (Time.time > nextCoracao2) {
					nextCoracao2 = Time.time + coracaoRate2;
					SpawnCoracaoColetavel ();

				}
				break;
			case "cena3":
				if (Time.time > nextCoracao3) {
					nextCoracao3 = Time.time + coracaoRate3;	
					SpawnCoracaoColetavel ();

				}
				break;
			case "cena4":
				if (Time.time > nextCoracao4) {
					nextCoracao4 = Time.time + coracaoRate4;
					SpawnCoracaoColetavel ();
						
				}
				break;
			default:
				break;
			}
		}

	}//Fecha Void Update

	void SpawnCoracaoColetavel () {
		Vector3 posicaoRandom1 = new Vector3 (Random.Range (-10.5f, 10.5f), transform.position.y, 10);
		Instantiate (coracaoColetavel, posicaoRandom1, transform.rotation); // Quaternion.identity é para o objeto ficar perfeitamente alinhado com o mundo (sem rotação).
	}

}
