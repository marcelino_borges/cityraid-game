﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaInimiga : MonoBehaviour {

	public Rigidbody2D rb;
	public float veloBala;
	private	Vector3 posicaoAtual;

	// Use this for initialization
	void Start () {
		veloBala = 600 * Time.fixedDeltaTime;
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		if (PauseMenu.gamePausado == false) {
			rb.velocity = transform.up * -veloBala;
			//rb.AddForce (new Vector2 (0,1)*-veloBala);

		} 
	}

	void OnTriggerEnter2D (Collider2D colls) {
		Destroy(gameObject);
	}
}
