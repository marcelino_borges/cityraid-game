﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms.Impl;
using System;

public class Inimigo2Controller : MonoBehaviour {

	public		Rigidbody2D	rb;
	public 		GameObject 	bala2;
	public	 	GameObject 	score;
	public		Transform	spawnScore;
	public 		Transform 	escape;
	public		Animator	Animador;
	public		AudioClip	AudioLevaDano;
	public		float		veloInimigo;
	private		int			Vida;
	private		int 		DanoBala2;
	private		int 		DanoBala3;
	private		int 		DanoBala4;
	private		float 		fireRate;
	private		float 		nextFire;
	private		float 		breakNextFire;
	private		bool 		podeSpawnScore;

	void Start () {		
		veloInimigo = UnityEngine.Random.Range (-300f,-150f) * Time.fixedDeltaTime * 20;
		Vida = 15;
		DanoBala2 = 5;
		DanoBala3 = 10;
		DanoBala4 = 15;
		fireRate = 0.8f; // Frequencia dos tiros
		nextFire = 0;
		podeSpawnScore = true;
	}

	void FixedUpdate () {
		if (PauseMenu.gamePausado == false) {
			rb.AddForce (new Vector2 (0, 1) * veloInimigo); // Movimenta o Inimigo

			if (Time.time > nextFire) {
				nextFire = Time.time + fireRate;
				Instantiate (bala2, escape.position, escape.rotation);
			}	

			if (Vida <= 0 && podeSpawnScore == true) {		
				if (AudioManager.instance.efxSource.enabled == true) {	
					AudioManager.instance.PlaySingle (AudioLevaDano, 1f);
				}
				Animador.SetTrigger ("morto");
				Instantiate (score, spawnScore.position, spawnScore.rotation);
				Destroy (gameObject, 0.2f);
				ScoreManager.score += 2;
				podeSpawnScore = false;			 
			} 
		} // if gamePausado
	} // Fecha Update

	void OnTriggerEnter2D (Collider2D colls) {
		switch (colls.gameObject.tag) {
		case "Player":	
			Animador.SetBool ("morto", true);
			if (AudioManager.instance.efxSource.enabled == true) {
				AudioManager.instance.PlaySingle (AudioLevaDano, 1f);
			}
			Destroy (gameObject, 0.2f); // Destrói a nave após a colisão com o player ou com a bala 
			break;
		case "Bala2":
			Vida -= DanoBala2; // Tira dano da vida do inimigo
			Animador.SetBool ("dano", true);
			Invoke ("sairDoDano", 0.1f);			 
			break;
		case "Bala3":
			Vida -= DanoBala3; // Tira dano da vida do inimigo
			Animador.SetBool ("dano", true);
			Invoke ("sairDoDano", 0.1f);
			break;
		case "Bala4":
			Vida -= DanoBala4; // Tira dano da vida do inimigo
			Animador.SetBool ("dano", true);
			Invoke ("sairDoDano", 0.1f);
			break;
		} //Switch			
	} //Fecha Void OnTriggerEnter2D

	void sairDoDano() {
		Animador.SetBool ("dano", false);
	}



} //Fecha classe