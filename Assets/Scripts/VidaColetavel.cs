﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Configuration;
using UnityStandardAssets.CrossPlatformInput;

public class VidaColetavel : MonoBehaviour {

	public			Animator		Animador;

	void Start () {
		
	}	

	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D colls) {
		if (colls.gameObject.tag == "Player") {
			if (PlayerController.VidaAtual < 100) {
				Animador.SetTrigger ("coletar");
			}
			Destroy (gameObject, 0.3f);
		}
	}


}
