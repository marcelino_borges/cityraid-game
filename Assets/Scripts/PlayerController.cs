﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Configuration;
using UnityStandardAssets.CrossPlatformInput;


public class PlayerController : MonoBehaviour {
	
	public 			Rigidbody2D			rb;
	private			Vector3 			velAtual;
	public 			Animator 			Animador;
	public 			GameObject 			bala2;
	public 			GameObject 			bala3;
	public 			GameObject 			bala4;
	public			GameObject			FireShot;
	public			GameObject			Inimigo2;
	public			GameObject			Inimigo1;
	public 			Transform 			escape;
	public			Slider				BarraDeVida;
	public			Animator 			ImagemDeDano;
	public			AudioClip			AudioTiro;
	public			AudioClip			GameOver;
	public  static 	PlayerController 	instance = null;
	public			SpriteRenderer 		PlayerSpriteRenderer;
	public	static	bool				PlayerAtivo;
	public 			bool 				PlayerVivo;
	public			bool 				PlayerVivoNaCam;
	private			bool 				gamePausado;
	private			int					DanoColisaoInimigo1; // Dano causado ao player pela colisão com a nave inimiga 1
	private			int					DanoColisaoInimigo2; // Dano causado ao player pela colisão com a nave inimiga 2
	private			int					DanoColisaoBala; // Dano causado ao player pela colisão com a bala da nave inimiga
	private			int					DanoColisaoBoss1; // Dano causado ao player pela colisão com o Boss 1
	public	static	int					VidaAtual;
	public 			float 				veloMove = 15; // Velocidade de movimento do Player
	private			float 				atkSpeed; // velocidade do surgimento das balas
	private			float 				nextFire; // contador temporário para dizer a hora do proximo tiro
	public	static	string				balaEmUso;


	void Start () {

		rb = GetComponent<Rigidbody2D> ();
		PlayerVivoNaCam = GameObject.Find("Main Camera").GetComponent<CameraController> ().PlayerVivo;

		Animador.SetBool ("parado", true);
		Animador.SetBool ("acelerando", false);
		Animador.SetBool ("viraEsquerda", false);
		Animador.SetBool ("viraDireita", false);
		Animador.SetBool ("morto", false);

		atkSpeed = 0.2f; 
		nextFire = 0;

		DanoColisaoInimigo1 = 10; // Dano a ser causado ao player pela colisão com a nave inimiga 1
		DanoColisaoInimigo2 = 20; // Dano a ser causado ao player pela colisão com a nave inimiga 2

		DanoColisaoBala = 10; // Dano a ser causado ao player pela colisão com a bala das naves inimigas
		DanoColisaoBoss1 = 30; // Dano causado ao player pela colisão com o Boss 1


		VidaAtual = 100;
		PlayerVivoNaCam = true;
		PlayerVivo = true;

		Physics2D.IgnoreCollision (Inimigo1.GetComponent <Collider2D>(), GetComponent <Collider2D>(),true);
		Physics2D.IgnoreCollision (Inimigo2.GetComponent <Collider2D>(), GetComponent <Collider2D>(),true);
		PlayerAtivo = true;

		balaEmUso = "bala2";


	}

	void FixedUpdate () {
		if (PauseMenu.gamePausado == false) {
			if (PlayerAtivo == true) {
				Movimentar ();			
				Atirar ();
				Morrer ();	
			}
		}
		BarraDeVida.value = VidaAtual;		

		//Pause para a Entrada do Boss1
		if (CameraController.posicaoCamera >= 152 & CameraController.posicaoCamera <= 157.8) {  // Checar no SpawnNavesController referente à pausa no spawn de naves a partir da posição.y 150
			
			PlayerAtivo = false;
		} else if (CameraController.posicaoCamera >= 157.9) {
			PlayerAtivo = true;
		}
	}

	void OnTriggerEnter2D (Collider2D colls) {
		switch (colls.gameObject.tag) {
		case "Inimigo1":
			VidaAtual -= DanoColisaoInimigo1;
			piscaPlayerDano();
			ImagemDeDano.SetBool ("imagemDano", true);
			Invoke ("imagemDanoFalse", 0.1f);
			break;
		case "Inimigo2":
			VidaAtual -= DanoColisaoInimigo2;
			piscaPlayerDano();
			ImagemDeDano.SetBool ("imagemDano", true);
			Invoke ("imagemDanoFalse", 0.1f);
			break;
		case "BalaInimiga":
			VidaAtual -= DanoColisaoBala;
			piscaPlayerDano();
			ImagemDeDano.SetBool ("imagemDano", true);
			Invoke ("imagemDanoFalse", 0.1f);
			break; 
		case "Boss1":
			VidaAtual -= DanoColisaoBoss1;
			piscaPlayerDano();
			ImagemDeDano.SetBool ("imagemDano", true);
			Invoke ("imagemDanoFalse", 0.1f);
			break;
		case "VidaColetavel":
			if (VidaAtual <= 50) {
				VidaAtual += 50;
			} else if (VidaAtual > 50) {
				VidaAtual = 100;
			}
			break;
		case "Bala2Coletavel":
			balaEmUso = "bala2";
			break;	
		case "Bala3Coletavel":
			balaEmUso = "bala3";
			break;	
		case "Bala4Coletavel":
			balaEmUso = "bala4";
			break;	
		default:			
			break;
		}//Switch
	}//Fecha Void OnCollisionEnter2D
		
	public void Atirar () {
			if (Input.GetKey (KeyCode.KeypadEnter) || CrossPlatformInputManager.GetButton ("FireButton")) {
				if (Time.time > nextFire) {
					nextFire = Time.time + atkSpeed;
					if (AudioManager.instance.efxSource.enabled == true) {
						AudioManager.instance.RandomizeSfx (AudioTiro, 0.3f);
					}
					Instantiate (FireShot, new Vector3 (escape.position.x, escape.position.y - 0.7f, escape.position.z), escape.rotation, transform);
					// Escolhe a bala a ser atirada, conforme coletável
					switch (balaEmUso) {
					case "bala2":
						Instantiate (bala2, escape.position, escape.rotation);
						break;
					case "bala3":
						Instantiate (bala3, escape.position, escape.rotation);
						break;
					case "bala4":
						Instantiate (bala4, escape.position, escape.rotation);
						break;
					default:
						break;
					} // Switch
				} // if nextfire
			}// if GetKey, GetButton		
	} //Fecha Void Atirar

	void Morrer () {
		if (VidaAtual <= 0) {
			Animador.SetBool ("morto", true);
			if (AudioManager.instance.efxSource.enabled == true) {
				AudioManager.instance.musicSource.Stop ();
				AudioManager.instance.PlaySingle (GameOver, 0.4f);
			}
			SceneManager.LoadScene ("GameOver");
		} 
	}

	public void Movimentar () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		float mobileHorizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
		float mobileVertical = CrossPlatformInputManager.GetAxis ("Vertical");

		Vector2 movimento = new Vector2 (moveHorizontal, moveVertical);
		Vector2 movimentoMobile = new Vector2 (mobileHorizontal, mobileVertical);

			//ANDAR
			rb.AddForce (movimento * veloMove); // Movimenta o Player
			rb.AddForce (movimentoMobile * veloMove);

			// ANIMAÇÕES ESQUREDA-DIREITA
			if (moveHorizontal > 0 || mobileHorizontal > 0) {
				Animador.SetBool ("viraEsquerda", false);
				Animador.SetBool ("viraDireita", true);
			} else if (moveHorizontal < 0 || mobileHorizontal < 0) {
				Animador.SetBool ("viraDireita", false);
				Animador.SetBool ("viraEsquerda", true);
			} else {
				Animador.SetBool ("viraDireita", false);
				Animador.SetBool ("viraEsquerda", false);
			}

			// ANIMAÇÃO DE ACELERANDO
			if (moveVertical > 0 || mobileVertical > 0) {
				Animador.SetBool ("acelerando", true);
			} else if (moveVertical < 0 || mobileVertical < 0) {
				Animador.SetBool ("acelerando", true);
			} else {
				Animador.SetBool ("acelerando", false);
			}		
	} // Void Movimentar()

	void imagemDanoFalse() {
		ImagemDeDano.SetBool ("imagemDano", false);
	}

	void playerLevaDano () {
		PlayerSpriteRenderer.enabled = false;
	}

	void playerSaiDoDano () {
		PlayerSpriteRenderer.enabled = true;
	}

	void piscaPlayerDano () {
		Invoke ("playerLevaDano", 0);
		Invoke ("playerSaiDoDano", 0.1f);
		Invoke ("playerLevaDano", 0.1f);
		Invoke ("playerSaiDoDano", 0.1f);

	}

} // Fecha Classe
