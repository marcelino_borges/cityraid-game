﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography.X509Certificates;
using UnityEngine.UI;

public class GetPlayerScore : MonoBehaviour {
	public int ScoreSalvo;

	public Text	text;

	// Use this for initialization
	void Awake () {
		text = GetComponent <Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		ScoreSalvo = PlayerPrefs.GetInt ("Player Score");
		if (ScoreSalvo < 10) {
			text.text = "0" + ScoreSalvo.ToString ();
		} else {
			text.text = ScoreSalvo.ToString ();
		}
	}
}
