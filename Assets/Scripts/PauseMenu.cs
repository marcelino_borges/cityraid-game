﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PauseMenu : MonoBehaviour  {

	public			Animator	AnimadorPauseMenu;
	public static	bool		gamePausado;


	void Awake () {
		gamePausado = false;
	}

	void Update () {
		
	}

	public void BtExit () {
		Application.Quit ();
	}

	public void BtPauseMenu() {
		if (AnimadorPauseMenu.GetBool ("gamePausado") == false) {
			AnimadorPauseMenu.SetBool ("gamePausado", true);
			gamePausado = true;
			if (AudioManager.instance.efxSource.enabled == true) {
				AudioManager.instance.musicSource.Pause ();
			}
		} else {
			AnimadorPauseMenu.SetBool ("gamePausado", false);
			gamePausado = false;
			if (AudioManager.instance.efxSource.enabled == true) {
				AudioManager.instance.musicSource.Play ();
			}
		}
	}

	public void BtMainMenu() {

		SceneManager.LoadScene ("MainMenu");
	}

	public void BtRestart() {
		Scene cenaAtual = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (cenaAtual.name);
	}

	public void BtAudio() {
		AudioManager.instance.MutarSons ();
	}
}
