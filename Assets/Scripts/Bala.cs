﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {

	public Rigidbody2D rb;
	public float veloBala;

	// Use this for initialization
	void Start () {
		veloBala = 800 * Time.deltaTime;
	}

	// Update is called once per frame
	void Update () {
		if (PauseMenu.gamePausado == false) {
			rb.velocity = transform.up * veloBala;
			Destroy (gameObject, 2);
		}
	}

	void OnTriggerEnter2D (Collider2D colls) {
		Destroy(gameObject);
	}
}
