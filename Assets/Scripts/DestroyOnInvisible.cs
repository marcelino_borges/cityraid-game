﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnInvisible : MonoBehaviour {

	/*
	void OnBecameInvisible () {
		Destroy (this.gameObject);
	} */

	void OnTriggerExit2D (Collider2D colls) {
		if (colls.gameObject.tag == "LimiteInferiorMapa") {			
			Destroy (gameObject);
		}
	}

	void OnBecameInvisible () {
		Destroy (gameObject);
	}

}
