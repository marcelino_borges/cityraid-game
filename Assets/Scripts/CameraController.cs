﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public			float		SpeedCamera;
	public	static	float		posicaoCamera;
	public	static	float 		posicaoFimDeMapa;
	public	Rigidbody2D			rb;
	public	bool 				PlayerVivo;


	void Start () {
		SpeedCamera = 1 * Time.deltaTime;

		posicaoFimDeMapa = 157.9f;
	}

	void FixedUpdate () {
		
		posicaoCamera = transform.position.y;

		if (PauseMenu.gamePausado == false) {
			if (transform.position.y < posicaoFimDeMapa) {
				posicaoCamera += SpeedCamera;
				transform.position = new Vector3 (transform.position.x, posicaoCamera, transform.position.z);
				Boss1Controller.Boss1Ativo = true;
			}


		} // If GamePausado
	} //Fecha void Update
} //Fecha classe
