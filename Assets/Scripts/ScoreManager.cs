﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {

	public static int score;
	Text text;

	// Use this for initialization
	void Awake () {
		text = GetComponent <Text> ();
		Scene cenaAtual = SceneManager.GetActiveScene ();
		if (cenaAtual.name == "cena1") {
			score = 0;
		}

	}


	
	// Update is called once per frame
	void Update () {
				
		if (score < 10) {
			text.text = "0" + score.ToString ();
		} else {
			text.text = score.ToString ();
		}

		SetScore ();
	}

	public static void SetScore () {
		PlayerPrefs.SetInt ("Player Score", score);
	}


}
